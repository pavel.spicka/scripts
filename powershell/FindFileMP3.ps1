#version 0.8
#2017
# 
############################################################################
#Script Variables
############################################################################
$Path       = "\\S\e$"
$MyFile     = "C:\script\month_script\temp\4.5.2._mp3.html"
$CutPath    = 14
$AddPath       = "E:\"
$WatchYouFind       = "*.mp3"
$LengthDomainNamePlusSlash   = 9 #\ = 9
$MyHostName     = ""
$Corporation    = ""
############################################################################
#Script Variables 
############################################################################
Set-Location -Path $Path
Remove-Item $MyFile

if($MyHostName.Length -eq 0) { $MyHostName = get-content env:computername }

$DirArray = @()
$CelkemSizeRound = 0
$CelkemSize = 0
$Owner = 0
$IdOwner = ""
$Id = 1

$StartHtml  = @"
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"&gt;
<html><head><title>Systems Report</title><meta http-equiv="refresh" content="120" />
<style type="text/css">
<!–
body {
word-wrap: break-word;
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

table{
word-wrap: break-word;
border-collapse: collapse;
border: 1;
font: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
color: black;
cellspacing: 1;
cellpadding: 1;
margin-bottom: 10px;
}

table td{
word-wrap: break-word;
font-size: 12px;
padding-left: 2px;
padding-right: 5px;
text-align: left;
background: white;
border-width: medium 1pt 1pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;

}

table th {
font-size: 12px;
font-weight: bold;
color: white;
padding-left: 0px;
padding-right: 20px;
text-align: left;
background: maroon;
border-width: medium 1pt 1.5pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;
}


.company  {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 10pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 8pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle_min {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 6pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 25px;
}

h1{ 
clear: both;
font-size: 24pt;
font-family: Tahoma;
font-weight: bold;
color: rgb(0, 0, 200);
}

h2{ 
clear: both;
font-size: 130%;
color:#354B5E;
}

h3{
clear: both;
font-size: 100%;
font-family: Tahoma;
margin-left: 20px;
margin-bottom: 1px;
margin-top: 15px;
color:#475F77;
}


p{ margin-left: 20px; font-size: 12px; }

table.list{ float: left; }

table.list td:nth-child(1){
font-weight: bold;
border-right: 1px grey solid;
text-align: right;
}

table.list td:nth-child(2){ padding-left: 7px; }
table tr:nth-child(even) td:nth-child(even){ background: #BBBBBB; }
table tr:nth-child(odd) td:nth-child(odd){ background: #F2F2F2; }
table tr:nth-child(even) td:nth-child(odd){ background: #DDDDDD; }
table tr:nth-child(odd) td:nth-child(even){ background: #E5E5E5; }
div.column { width: 320px; float: left; }
div.first{ padding-right: 20px; border-right: 1px grey solid; }
div.second{ margin-left: 30px; }
table{ margin-left: 20px; }
–>
</style>
</head>
<tbody><center><table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;">
"@ | Out-File $MyFile -Append

"<h1>"+ $Corporation + "</h1><h2>" + $MyHostName + " - "  + $WatchYouFind + " files </h2>"| Out-File $MyFile -Append
"<h2>"+(Get-Date -Format d)+"</h2>"| Out-File $MyFile -Append
"<tr bgcolor="+'"'+"#999900"+'"'+"><td width="+'"'+"5%"+'"'+" ></td><td>FOLDER</td><td  width="+'"'+"9%"+'"'+"  >MODIF</td><td width="+'"'+"10%"+'"'+" >SIZE</td><td width="+'"'+"12%"+'"'+" >IDOWNER</td><td width="+'"'+"13%"+'"'+" >OWNER</td></tr>"| Out-File $MyFile -Append

$NameDomain = Get-ADDomain | Select-Object NetBIOSName

$DirArray = Get-ChildItem -Exclude $Path -Recurse -Path $WatchYouFind | Select-Object LastWriteTime, Length, Name, Directory, FullName , Owner
ForEach ($i in $DirArray)
{
   $MyFileName = $i.Name
   $MyPath = $AddPath  + $i.FullName.Substring($CutPath)
   $Modif = $i.LastWriteTime

   $MySize = ([math]::Round(($i.Length/1MB), 2))

   $CelkemSize = $CelkemSize + $i.Length

   $Owner = (Get-Acl $i.FullName).Owner

   $IdOwner = (Get-ADUser -identity $Owner.Substring($LengthDomainNamePlusSlash) -server example.biz).GivenName  + " " + ((Get-ADUser -identity $Owner.Substring($LengthDomainNamePlusSlash) -server epcos.biz).Surname)
   
   if($IdOwner -eq "" ){$Owner = ""}
   
   $Export  = "<tr><td width="+'"'+"5%"+'"'+" >$Id.</td><div class=breakall><td>$MyPath</td></div><td width="+'"'+"9%"+'"'+" >$Modif</td><td width="+'"'+"10%"+'"'+" >$MySize MB</td><td width="+'"'+"12%"+'"'+" >$IdOwner</td><td width="+'"'+"13%"+'"'+" >$Owner</td></tr>"| Out-File $MyFile -Append
   $Id ++
   $IdOwner = ""
}
$CelkemSizeRound = ([math]::Round(($CelkemSize/1MB), 2))
$CeolemSizeFile  = "<tr><td width="+'"'+"5%"+'"'+" ></td><td></td><td width="+'"'+"9%"+'"'+" >Celkem</td><td width="+'"'+"10%"+'"'+" >$CelkemSizeRound MB</td><td width="+'"'+"12%"+'"'+" ></td><td width="+'"'+"13%"+'"'+" ></td></tr>" | Out-File $MyFile -Append 
$EndHtml    = @" 
</table>   
        <h3>Created by</h3>
        <div class=subtitle_min> Report HTML - FindFile V0.8</div>
        <div class=subtitle_min></div>
        </center>
    </tbody>
</html>
"@ | Out-File $MyFile -Append 
Invoke-Item $MyFile
