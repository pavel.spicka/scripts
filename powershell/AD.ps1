﻿############################################################################
#version 0.8
#2017
# 
############################################################################
#Script Variables
############################################################################
$MyFile     = "C:\script\month_script\temp\2.1.1.Domain_users.html"
$Corporation    = ""
############################################################################
Set-Location -Path $Path
Remove-Item $MyFile
$id = 0
$EPPraha_C = 0
$EPMoravicany_C = 0
$EPMAG_C = 0
$EPPTC_C = 0
$EPOs_C = 0
$SKLOCAL_C = 0

if($MyHostName.Length -eq 0) { $MyHostName = get-content env:computername }

$StartHtml  = @"
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"&gt;
<html><head><title>Systems Report</title><meta http-equiv="refresh" content="120" />
<style type="text/css">
<!–
body {
word-wrap: break-word;
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

table{
word-wrap: break-word;
border-collapse: collapse;
border: 1;
font: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
color: black;
cellspacing: 1;
cellpadding: 1;
margin-bottom: 10px;
}

table td{
word-wrap: break-word;
font-size: 12px;
padding-left: 2px;
padding-right: 5px;
text-align: left;
border-width: medium 1pt 1pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;

}

table th {
font-size: 12px;
font-weight: bold;
padding-left: 0px;
padding-right: 20px;
text-align: left;
border-width: medium 1pt 1.5pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;
}


.company  {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 10pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 8pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle_min {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 6pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 25px;
}

h1{ 
clear: both;
font-size: 24pt;
font-family: Tahoma;
font-weight: bold;
color: rgb(0, 0, 200);
}

h2{ 
clear: both;
font-size: 130%;
color:#354B5E;
}

h3{
clear: both;
font-size: 100%;
font-family: Tahoma;
margin-left: 20px;
margin-bottom: 1px;
margin-top: 15px;
color:#475F77;
}


p{ margin-left: 20px; font-size: 12px; }

table.list{ float: left; }

table.list td:nth-child(1){
font-weight: bold;
border-right: 1px grey solid;
text-align: right;
}

table.list td:nth-child(2){ padding-left: 7px; }
div.column { width: 320px; float: left; }
div.first{ padding-right: 20px; border-right: 1px grey solid; }
div.second{ margin-left: 30px; }
table{ margin-left: 20px; }
–>
</style>
</head>
<body><center>
"@ | Out-File $MyFile -Append

"<h2>Domain Users</h2>"| Out-File $MyFile -Append
"<h2>"+(Get-Date -Format d)+"</h2>"| Out-File $MyFile -Append
############################################################################
#
############################################################################
@"
 <h2></h2>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <tr><td width="3%" >ID</td><td width="6%">UID</td><td width="17%">DISPLAY NAME</td><td width="12%">EMAIL ADDRESS</td><td width="3%">COST C.</td><td width="10%">DEPARTMENT</td><td width="10%">CREATED</td><td width="8%">CITY</td><td width="8%">MODIFIED</td><td width="8%">LAST LOGON</td></tr>
 
"@ | Out-File $MyFile -Append
$id = 0
$DirArray = Get-ADUser -Filter {extensionAttribute13 -eq "MAG"} -Server e.biz -SearchBase "OU=Users,OU=,OU=cz_SK,DC=,DC=" -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City, st, extensionAttribute13
    ForEach ($i in $DirArray)
    {

        $EPMAG_C = $EPMAG_C + 1

        $id = $id + 1
        $Name = $i.GivenName +" "+ $i.Surname
        $SamAccountName = $i.SamAccountName
        $GivenName = $i.GivenName
        $DistinguishedName = $i.DistinguishedName
        $UserPrincipalName = $i.UserPrincipalName
        $LastLogon = $i.LastLogonDate
        $st = $i.st
        $Modified = $i.Modified
        $DisplayName = $i.DisplayName
        $Created = $i.Created
        $Department = $i.Department
        $City = $i.City

        "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
    }

############################################################################
#
############################################################################
@"
</table>
 <h2></h2>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <tr><td width="3%" >ID</td><td width="6%">UID</td><td width="17%">DISPLAY NAME</td><td width="12%">EMAIL ADDRESS</td><td width="3%">COST C.</td><td width="10%">DEPARTMENT</td><td width="10%">CREATED</td><td width="8%">CITY</td><td width="8%">MODIFIED</td><td width="8%">LAST LOGON</td></tr>
 
"@ | Out-File $MyFile -Append
$id = 0
$DirArray = Get-ADUser -Filter {extensionAttribute13 -eq "PPD"} -Server e.biz -SearchBase "OU=Users,OU=,OU=CZ_SK,DC=,DC=" -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City, st, extensionAttribute13
    ForEach ($i in $DirArray)
    {

        $EPPTC_C = $EPPTC_C + 1

        $id = $id + 1
        $Name = $i.GivenName +" "+ $i.Surname
        $SamAccountName = $i.SamAccountName
        $GivenName = $i.GivenName
        $DistinguishedName = $i.DistinguishedName
        $UserPrincipalName = $i.UserPrincipalName
        $LastLogon = $i.LastLogonDate
        $st = $i.st
        $Modified = $i.Modified
        $DisplayName = $i.DisplayName
        $Created = $i.Created
        $Department = $i.Department
        $City = $i.City

        "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
    }

############################################################################
#Ostatní
############################################################################
@"
</table>
 <h2></h2>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <tr><td width="3%" >ID</td><td width="6%">UID</td><td width="17%">DISPLAY NAME</td><td width="12%">EMAIL ADDRESS</td><td width="3%">COST C.</td><td width="10%">DEPARTMENT</td><td width="10%">CREATED</td><td width="8%">CITY</td><td width="8%">MODIFIED</td><td width="8%">LAST LOGON</td></tr>
 
"@ | Out-File $MyFile -Append
$id = 0
$DirArray = Get-ADUser -Filter {extensionAttribute13 -eq "SPOL"} -Server .biz -SearchBase "OU=Users,OU=LSP01,OU=CZ_SK,DC=,DC=biz" -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City, st,extensionAttribute13
    ForEach ($i in $DirArray)
    {
            
            $EPOs_C = $EPOs_C + 1

            $id = $id + 1
            $Name = $i.GivenName +" "+ $i.Surname
            $SamAccountName = $i.SamAccountName
            $GivenName = $i.GivenName
            $DistinguishedName = $i.DistinguishedName
            $UserPrincipalName = $i.UserPrincipalName
            $LastLogon = $i.LastLogonDate
            $st = $i.st
            $Modified = $i.Modified
            $DisplayName = $i.DisplayName
            $Created = $i.Created
            $Department = $i.Department
            $City = $i.City

            "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
    }
$DirArray = Get-ADUser -Filter {extensionAttribute13 -eq "External"} -Server e.biz -SearchBase "OU=Users,OU=LSP01,OU=CZ_SK,DC=,DC=biz" -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City, st,extensionAttribute13
    ForEach ($i in $DirArray)
    {
            
            $EPOs_C = $EPOs_C + 1

            $id = $id + 1
            $Name = $i.GivenName +" "+ $i.Surname
            $SamAccountName = $i.SamAccountName
            $GivenName = $i.GivenName
            $DistinguishedName = $i.DistinguishedName
            $UserPrincipalName = $i.UserPrincipalName
            $LastLogon = $i.LastLogonDate
            $st = $i.st
            $Modified = $i.Modified
            $DisplayName = $i.DisplayName
            $Created = $i.Created
            $Department = $i.Department
            $City = $i.City

            "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
    }

############################################################################

############################################################################
@"
</table>
 <h2></h2>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <tr><td width="3%" >ID</td><td width="6%">UID</td><td width="17%">DISPLAY NAME</td><td width="12%">EMAIL ADDRESS</td><td width="3%">COST C.</td><td width="10%">DEPARTMENT</td><td width="10%">CREATED</td><td width="8%">CITY</td><td width="8%">MODIFIED</td><td width="8%">LAST LOGON</td></tr>
 
"@ | Out-File $MyFile -Append
$id = 0
$DirArray = Get-ADUser -Filter * -Server e.biz -SearchBase "OU=Users,OU=LSP01,OU=CZ_MV,DC=,DC=biz" -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City, st,extensionAttribute13
    ForEach ($i in $DirArray)
    {
        $EPMoravicany_C = $EPMoravicany_C + 1

        $id = $id + 1
        $Name = $i.GivenName +" "+ $i.Surname
        $SamAccountName = $i.SamAccountName
        $GivenName = $i.GivenName
        $DistinguishedName = $i.DistinguishedName
        $UserPrincipalName = $i.UserPrincipalName
        $LastLogon = $i.LastLogonDate
        $st = $i.st
        $Modified = $i.Modified
        $DisplayName = $i.DisplayName
        $Created = $i.Created
        $Department = $i.Department
        $City = $i.City

        "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
    }
############################################################################
#
############################################################################
@"
</table>
 <h2></h2>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <tr><td width="3%" >ID</td><td width="6%">UID</td><td width="17%">DISPLAY NAME</td><td width="12%">EMAIL ADDRESS</td><td width="3%">COST C.</td><td width="10%">DEPARTMENT</td><td width="10%">CREATED</td><td width="8%">CITY</td><td width="8%">MODIFIED</td><td width="8%">LAST LOGON</td></tr>
 
"@ | Out-File $MyFile -Append
$id = 0
$DirArray = Get-ADUser -Filter * -Server example.biz  -properties * | Select-Object DistinguishedName, Enable, GivenName, Name, ObjectClass, ObjectGUID, SamAccountName, Surname, UserPrincipalName , LastLogonDate , Modified , logonCount, DisplayName, Created, Department, City ,st ,extensionAttribute13
ForEach ($i in $DirArray)
{
        $SKLOCAL_C = $SKLOCAL_C + 1

        $id = $id + 1
        $Name = $i.GivenName +" "+ $i.Surname
        $SamAccountName = $i.SamAccountName
        $GivenName = $i.GivenName
        $DistinguishedName = $i.DistinguishedName
        $UserPrincipalName = $i.UserPrincipalName
        $LastLogon = $i.LastLogonDate
        $st = $i.st
        $Modified = $i.Modified
        $DisplayName = $i.DisplayName
        $Created = $i.Created
        $Department = $i.Department
        $City = $i.City

        "<tr><td>$id</td><td>$SamAccountName</td><td>$Name</td><td>$UserPrincipalName</td><td>$st</td><td>$Department</td><td>$Created</td><td>$City</td><td>$Modified</td><td>$LastLogon</td></tr>"| Out-File $MyFile -Append
}

$EPPraha_D = ($EPPraha_old = 0) -$EPPraha_C
$EPMoravicany_D = ($EPMoravicany_old = 20) - $EPMoravicany_C 
$EPMAG_D = ($EPMAG_old = 142) - $EPMAG_C
$EPPTC_D = ($EPPTC_old = 294) - $EPPTC_C
$EPOs_D = ($EPOs_old = 105) - $EPOs_C
$SKLOCAL_D = ($SKLOCAL_old = 183) - $SKLOCAL_C

$Summary = ($EPPraha_C + $EPMoravicany_C + $EPMAG_C + $EPPTC_C + $EPOs_C + $SKLOCAL_C)

@" 
</table>
 <table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;"> 
 <h2>Summary</h2>
 <tr><td>DOMAIN USER</td><td width="+'"'+"20%"+'"'+" >COUNT</td><td width="+'"'+"20%"+'"'+" >DIF</td></tr>
"@ | Out-File $MyFile -Append

 "<tr><td></td><td>0</td><td>0</td></tr>"| Out-File $MyFile -Append
 "<tr><td></td><td>$EPMoravicany_C</td><td></td></tr>"| Out-File $MyFile -Append
 "<tr><td></td><td>$EPMAG_C</td><td></td></tr>"| Out-File $MyFile -Append
 "<tr><td></td><td>$EPPTC_C</td><td></td></tr>"| Out-File $MyFile -Append
 "<tr><td></td><td>$EPOs_C</td><td></td></tr>"| Out-File $MyFile -Append
 "<tr><td></td><td>$SKLOCAL_C</td><td></td></tr>"| Out-File $MyFile -Append
 "<tr><td>SUMMARY</td><td></td></tr>"| Out-File $MyFile -Append

$EndHtml = @" 
</table>   
        <h3></h3>
        <div class=subtitle_min>Report HTML - Domain Users</div>
        <div class=subtitle_min></div>
        </center>
    </body>
</html>
"@ | Out-File $MyFile -Append  
Invoke-Item $MyFile
