﻿<#
-Dcom.sun.enterprise.security.httpsOutboundKeyAlias=myalias


  <protocol name="http-listener-2" security-enabled="true">
    <http max-connections="250" default-virtual-server="server">
      <file-cache></file-cache>
    </http>
    <ssl classname="com.sun.enterprise.security.ssl.GlassfishSSLImpl" cert-nickname="myalias"></ssl>
  </protocol>
#>

Write-Output "GlassFish 5 instalation"
Invoke-WebRequest -Uri "http://download.oracle.com/glassfish/5.0/release/glassfish-5.0.zip" -OutFile "C:\temp\glassfish-5.0.zip"
Expand-Archive -Path C:\temp\glassfish-5.0.zip -DestinationPath C:\temp
Remove-Item -Path C:\temp\glassfish-5.0.zip
Move-Item "C:\temp\glassfish5" "C:\Program Files" 

Install-WindowsFeature Net-Framework-Core -source \\network\share\sxs

Write-Output "GlassFish 5 configuration"

Start-Process -Wait "C:\Program Files\glassfish5\bin\asadmin.bat" -ArgumentList "delete-domain domain1" -PassThru
#Start-Process -Wait "C:\Program Files\glassfish5\bin\asadmin.bat" -ArgumentList "create-domain --instanceport 80 --nopassword --adminport 4848 --domainproperties http.ssl.port=443 --keytooloptions CN=  mpogf5" -PassThru
Start-Process -Wait "C:\Program Files\glassfish5\bin\asadmin.bat" -ArgumentList "create-domain --instanceport 80 --nopassword --adminport 4848 --domainproperties http.ssl.port=443 mpogf5" -PassThru
Start-Process -Wait "C:\Program Files\glassfish5\bin\asadmin.bat" -ArgumentList "stop-domain mpogf5" -PassThru

Write-Output "Service na mame is : mpogf5"
Start-Process -Wait "C:\Program Files\glassfish5\bin\asadmin.bat" -ArgumentList "create-service --name mpogf5" -PassThru
Write-Output "Default password is keystore.jks : changeit"

Start-Process -Wait -FilePath "C:\Program Files\Java\jre1.8.0_221\bin\keytool.exe" -ArgumentList "-import -v -trustcacerts -alias root -file addtrustexternalcaroot.crt -keystore C:\Program Files\glassfish5\glassfish\domains\mpogf5\config\cacerts.jks"
Start-Process -Wait -FilePath "C:\Program Files\Java\jre1.8.0_221\bin\keytool.exe" -ArgumentList "-import -v -trustcacerts -alias mporga -file AddTrustCA.crt -keystore C:\Program Files\glassfish5\glassfish\domains\mpogf5\config\keystore.jks"

Start-Service -Name "mpogf5"

#$service = Get-WmiObject -Class Win32_Service -Filter "Name='mpogf5'"
#$service.delete()