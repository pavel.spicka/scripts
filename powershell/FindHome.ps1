#version 0.8
#2017
# 
############################################################################
#Script Variables
############################################################################
$Path       = "\\S\e$"
$MyFile     = "C:\script\month_script\temp\4.5.1._home.html"
$LengthDomainNamePlusSlash   = 9 #\ = 9
$MyHostName     = ""
$Corporation    = ""
############################################################################
#Script Variables 
############################################################################
Set-Location -Path $Path
Remove-Item $MyFile

if($MyHostName.Length -eq 0) { $MyHostName = get-content env:computername }
$DirArray = @()

$StartHtml  = @"
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd"&gt;
<html><head><title>Systems Report</title><meta http-equiv="refresh" content="120" />
<style type="text/css">
<!–
body {
word-wrap: break-word;
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

table{
word-wrap: break-word;
border-collapse: collapse;
border: 1;
font: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
color: black;
cellspacing: 1;
cellpadding: 1;
margin-bottom: 10px;
}

table td{
word-wrap: break-word;
font-size: 12px;
padding-left: 2px;
padding-right: 5px;
text-align: left;
border-width: medium 1pt 1pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;

}

table th {
font-size: 12px;
font-weight: bold;
padding-left: 0px;
padding-right: 20px;
text-align: left;
border-width: medium 1pt 1.5pt medium;
border-style: none solid solid none;
border-color: currentColor black black currentColor;
border-image: none;
}


.company  {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 10pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 8pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 20px;
}

.subtitle_min {
style=color: rgb(0, 0, 0);
font-family: Tahoma;
font-size: 6pt;
margin-top: 2px;
margin-bottom: 2px;
margin-left: 25px;
}

h1{ 
clear: both;
font-size: 24pt;
font-family: Tahoma;
font-weight: bold;
color: rgb(0, 0, 200);
}

h2{ 
clear: both;
font-size: 130%;
color:#354B5E;
}

h3{
clear: both;
font-size: 100%;
font-family: Tahoma;
margin-left: 20px;
margin-bottom: 1px;
margin-top: 15px;
color:#475F77;
}


p{ margin-left: 20px; font-size: 12px; }

table.list{ float: left; }

table.list td:nth-child(1){
font-weight: bold;
border-right: 1px grey solid;
text-align: right;
}

table.list td:nth-child(2){ padding-left: 7px; }
div.column { width: 320px; float: left; }
div.first{ padding-right: 20px; border-right: 1px grey solid; }
div.second{ margin-left: 30px; }
table{ margin-left: 20px; }
–>
</style>
</head>
<tbody><center><table width="1000px" border="2" style="table-layout: fixed; border-collapse: collapse;">
"@ | Out-File $MyFile -Append

"<h2>"+$MyHostName+" - HOME</h2>"| Out-File $MyFile -Append
"<h2>"+(Get-Date -Format d)+"</h2>"| Out-File $MyFile -Append
"<tr><td width="+'"'+"2%"+'"'+" ></td><td width="+'"'+"6%"+'"'+" >PERSON NAME</td><td width="+'"'+"4%"+'"'+" >SHAREFOLDER</td><td width="+'"'+"1%"+'"'+" >SIZE</td></tr>"| Out-File $MyFile -Append

$Id = 1
$CelkemSizeDirektory = 0
$CelkemSizeDirektoryRound = 0

$DirArray = Get-ChildItem -Path $Path -Directory | Select-Object LastWriteTime, Name, Directory, FullName , Owner
ForEach ($i in $DirArray)
{
   $Directory = $AddPath  + $i.FullName.Substring($CutPath)
   $Modif = $i.LastWriteTime

   $SizeDirektory = (Get-ChildItem $i.FullName -Recurse -Force -ErrorAction SilentlyContinue | Measure-Object -Property Length -Sum).Sum
   $SizeDirektoryRound = [math]::Round($SizeDirektory / 1MB)

   $CelkemSizeDirektory = $CelkemSizeDirektory + $SizeDirektory

   $ShareFolder = $i.Name
   $PersonName = (Get-ADUser -identity $ShareFolder -server example.biz).GivenName  + " " + ((Get-ADUser -identity $ShareFolder -server epcos.biz).Surname)
   if($PersonName.Length -eq 0) {
        if($SizeDirektoryRound -ge 500) {
                $Export  = "<tr><td>$Id.</td><td bgcolor= "+'"'+"#FFFF66"+'"'+">$PersonName</td><td>$ShareFolder</td><td bgcolor= "+'"'+"#FF0066"+'"'+">$SizeDirektoryRound MB</td></tr>"| Out-File $MyFile -Append
        }
        else {
                $Export  = "<tr><td>$Id.</td><td bgcolor= "+'"'+"#FFFF66"+'"'+">$PersonName</td><td>$ShareFolder</td><td>$SizeDirektoryRound MB</td></tr>"| Out-File $MyFile -Append
        }
   }
   else{
        if($SizeDirektoryRound -ge 500) {
                $Export  = "<tr><td>$Id.</td><td>$PersonName</td><td>$ShareFolder</td><td bgcolor= "+'"'+"#FF0066"+'"'+">$SizeDirektoryRound MB</td></tr>"| Out-File $MyFile -Append
        }
        else {
                $Export  = "<tr><td>$Id.</td><td>$PersonName</td><td>$ShareFolder</td><td>$SizeDirektoryRound MB</td></tr>"| Out-File $MyFile -Append
        }
   }

   $Id ++
   $PersonName = ""
}
$CelkemSizeDirektoryRound = [math]::Round(($CelkemSizeDirektory / 1MB))
$CeolemSizeFile  = "<tr><td></td><td></td><td>Celkem</td><td>$CelkemSizeDirektoryRound MB</td></tr>" | Out-File $MyFile -Append 
$CeolemSizeFile  = "<tr><td>Bigger than 500Mb is the red color</td></tr>" | Out-File $MyFile -Append 
$CeolemSizeFile  = "<tr><td>Unknown user is the yellow color</td></tr>" | Out-File $MyFile -Append 
$EndHtml    = @" 
</table>   
        <h3>Created by</h3>
        <div class=subtitle_min> Report HTML - FindFile V0.8</div>
        <div class=subtitle_min></div>
        </center>
    </tbody>
</html>
"@ | Out-File $MyFile -Append  
Invoke-Item $MyFile
