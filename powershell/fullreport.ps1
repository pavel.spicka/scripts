$head = @"
<html><head><title>Systems Report</title><meta http-equiv="refresh" content="120" />
<style type="text/css">
<!�
body {
font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
}

#report { width: 835px; }

table{
border-collapse: collapse;
border: none;
font: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
color: black;
margin-bottom: 10px;
}

table td{
font-size: 12px;
padding-left: 0px;
padding-right: 20px;
text-align: left;
}

table th {
font-size: 12px;
font-weight: bold;
padding-left: 0px;
padding-right: 20px;
text-align: left;
}

h2{ clear: both; font-size: 130%;color:#354B5E; }

h3{
clear: both;
font-size: 75%;
margin-left: 20px;
margin-top: 30px;
color:#475F77;
}

p{ margin-left: 20px; font-size: 12px; }

table.list{ float: left; }

table.list td:nth-child(1){
font-weight: bold;
border-right: 1px grey solid;
text-align: right;
}

table.list td:nth-child(2){ padding-left: 7px; }
table tr:nth-child(even) td:nth-child(even){ background: #BBBBBB; }
table tr:nth-child(odd) td:nth-child(odd){ background: #F2F2F2; }
table tr:nth-child(even) td:nth-child(odd){ background: #DDDDDD; }
table tr:nth-child(odd) td:nth-child(even){ background: #E5E5E5; }
div.column { width: 320px; float: left; }
div.first{ padding-right: 20px; border-right: 1px grey solid; }
div.second{ margin-left: 30px; }
table{ margin-left: 20px; }
�>
</style>
</head>

�@

Import-Module ".\setcolor.ps1"

$ComputerName= @(gc .\computers.txt)

foreach ($ComputerName in $ComputerName) {

$MyFile = "\\s\c$\inetpub\wwwroot\wiki\data\pages\999_deni_kontrola\"+($Computername.ToLower()).Substring(0,8)+".txt"
Remove-Item $MyFile

$PercentFree = Get-WmiObject Win32_LogicalDisk -ComputerName $Computername |  
 Where-Object { $_.DriveType -eq "3" } | Select-Object SystemName,VolumeName,DeviceID, 
 @{ Name = "Size (GB)" ; Expression = { "{0:N1}" -f( $_.Size / 1gb) } }, 
 @{ Name = "Free Space (GB)" ; Expression = {"{0:N1}" -f( $_.Freespace / 1gb ) } }, 
 @{ Name = "Percent Free" ; Expression = { "{0:P0}" -f( $_.FreeSpace / $_.Size ) } } | 
 ConvertTo-Html -Fragment -As Table -PreContent "<h2>Available Disk Space</h2>" | set-cellcolor -Property "percent free" red -filter "Percent free -lt '10%'" | Out-String

 $Restarted = Get-WmiObject -Class Win32_OperatingSystem -ComputerName $Computername | 
 Select-Object Caption,CSName, 
 @{ Name = "Last Restarted On" ; Expression = { $_.Converttodatetime( $_.LastBootUpTime ) } }| 
 ConvertTo-Html -Fragment -As Table -PreContent "<h2>Last Boot Up Time</h2>" | Out-String

 $Stopped = Get-WmiObject -Class Win32_Service -ComputerName $Computername | 
 Where-Object { ($_.StartMode -eq "Auto") -and ($_.State -eq "Stopped") } | 
 Select-Object SystemName, DisplayName, Name, StartMode, State, Description | 
 ConvertTo-Html -Fragment -PreContent "<h2>Services currently stopped that are set to autostart</h2>"| Out-String

 $now = get-date 
$subtractDays = New-Object System.TimeSpan 1,0,0,0,0 
$then = $Now.Subtract($subtractDays) 

 $systemErrors = Get-EventLog -Computername $computername -Logname "System" -After $then -Before $now -EntryType Error,Warning | 
select EntryType,MachineName,EventID,Message,Source,TimeGenerated |
ConvertTo-Html -Fragment -PreContent "<h2>System events</h2>"| set-cellcolor -Property EntryType -Color red -Filter "EntryType -eq 'Error'" | set-cellcolor -Property EntryType -Color yellow -Filter "EntryType -eq 'Warning'" | Out-String

 $AppErrors = Get-EventLog -Computername $computername -LogName "Application" -After $then -Before $now -EntryType Error,Warning | 
select EntryType,MachineNameEventID,Message,Source,TimeGenerated |
ConvertTo-Html -Fragment -PreContent "<h2>Application events</h2>"| set-cellcolor -Property EntryType -Color red -Filter "EntryType -eq 'Error'" | set-cellcolor -Property EntryType -Color yellow -Filter "EntryType -eq 'Warning'" | Out-String

$head_info =  "<h1>PowerShell Reporting " + $Computername + "</h1><br>" + "<h2>Tento report byl spusten:"+ $(Get-Date)  +"</h2><br>"
 
$Report = $head + $head_info + $PercentFree + $Restarted + $Services + $Stopped + $systemErrors + $AppErrors + "</body></html>"| Out-File  $MyFile -Encoding "UTF8"
 }
 